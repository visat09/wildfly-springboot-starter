## Wildfly Spring Boot Starter

- SpringBootFramwork 1.5.9.RELEASE
- tested with Wildfly-10.1.0.Final

## How to setup project
```
$ git clone https://gitlab.com/visat09/wildfly-springboot-starter
```

```
$ mvn install
```

## Context Path
```
http://localhost:8080/SpringBoot/
```

You can edit context-root in `WEB-INF/jboss-web.xml`
```xml
<?xml version="1.0" encoding="UTF-8"?>
<jboss-web>
    <context-root>/SpringBoot</context-root>
</jboss-web>
```
