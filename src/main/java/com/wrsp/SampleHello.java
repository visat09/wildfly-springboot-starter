package com.wrsp;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleHello {

    @RequestMapping("/")
    public String hello() {
        return "Hello World!";
    }

}
